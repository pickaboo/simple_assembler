#include "commands.h"
/*
 writer: niv elzam 036059194
 assumption/remark:
 1. the input can not be over 2^20-1 because the presentation off any larger number can not fit in 20 bit.
 2. I use linked list structure for symbol table and external symbol table. so the size is not limited but meaningless above 2^20.
 3. i have 8 global variables, but 2 of them (commands[]  and register[] in file commands.c) I reach indirectly, i.e, via functions.
    all of the global variables are defined in commands.c and with distinct names to avoid confusion.
 4.    
 
 the algorithm:
 it is similar to the one offered in the course notebook except small changes.
 
 guidelines:
 1.the program is divided to several segments:
	a.user interface - include main.c
	b.utility functions - include service.c . all the function in this file  are called from a action.c function. 
	  the purpose is to process small  task from function
	  in actions.c need to be done (like small manipulation in string  or numbers).
	c.implementation functions - action.c 
	d.define parts - commands.c and commands.h
	e. printing parts- prints.c
 */

int main(int argc,char *argv[])
{
	while(--argc)
		openFile(argv[argc]);
	return 0;
}
