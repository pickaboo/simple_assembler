#include "commands.h"
/*
 this file contain all the function that involve printing.
 */
extern int error;
extern Nptr symbolTree;
extern Nptr exSymbolTree;
extern  int DC;
extern int IC;
extern int lineNum;

void printDataOrString(FILE *f)
{
	
	int j;
	int i;
	char fourString1[11];/*the address*/
	char fourString2[11];/*the char or integer four base presentation.*/
	char *s=NULL;
	char s2[MAX_WORD];
	Nptr p=symbolTree;
	while(p)
	{
		if(strcmp((p->type),".string")==0)/*print the node properties which can be several lines.*/
		{
			s=p->content;
			for(i=0,j=0;s[i];i++)
			{
				{
					intToFourString(p->address+100+j,fourString1);
					intToFourString(s[i],fourString2);
					if(s[i]!='\"')
					{
						fprintf(f,"%s\t\t%s\n",getPastZeroes(fourString1),fourString2);
						j++;
					}		
				}
			}
			intToFourString(p->address+100+j,fourString1);
			intToFourString(0,fourString2);/*for the \0*/
			fprintf(f,"%s\t\t%s\n",getPastZeroes(fourString1),fourString2);
		}
		else if(strcmp((p->type),".data")==0)/*print the node properties which can be several lines.*/
		{
			j=0;
			strcpy(s2,p->content);
			s=strtok(s2," ,");
			while(s)
			{
				intToFourString(p->address+100+j,fourString1);
				intToFourString(atoi(s),fourString2);
				fprintf(f,"%s\t\t%s\n",getPastZeroes(fourString1),fourString2);
				j++;
				s=strtok(NULL," ,");
			}
		}
		p=p->next;
	}
	
}

void setOperands(char s[],FILE *files[])
{
	
	enum fl{ob,ent,ext};
	char word[MAX_WORD];
	char fourString1[11];
	char fourString2[11];
	Nptr np=NULL;
	Nptr np1=NULL;
	Register *rp=NULL;
	int flag=1;
	
	do{	
		getStringInOutBraces(s,word,flag);/*if s is addressing method 2*/
		if(isInt(word))
		{
			IC++;
			intToFourString(IC+100,fourString1);
			intToFourString(atoi(word),fourString2);
			fprintf(files[0],"%s\t\t%s\t\t%c\n",getPastZeroes(fourString1),fourString2,'a');/*in the .ob file*/
		}
		else if(np=findInTree(&symbolTree,word))
		{
			IC++;
			intToFourString(np->address+100,fourString2);
			intToFourString(IC+100,fourString1);
			fprintf(files[0],"%s\t\t%s\t\t%c\n",getPastZeroes(fourString1),fourString2,'r');/*in the .ob file*/
			if(np1=findInTree(&exSymbolTree,word))
			{
				if(strcmp(np1->type,".entry")==0 && strcmp(np1->content,"")==0)/*mark in content member if it has been printed in the files (because .string and .data  location )*/
					fprintf(files[1],"%s\t\t%s\n",word,getPastZeroes(fourString2));/*in the .ent file*/
				else if(strcmp(np1->type,".extern")==0 && strcmp(np1->content,"")==0)
						fprintf(files[2],"%s\t\t%s\n",word,getPastZeroes(fourString2));/*in the .ext file*/
				np1->content[0]='1';
				np1->content[1]='\0';
			}
			
		}
		else if(np=findInTree(&exSymbolTree,word))
		{
			
			IC++;
			intToFourString(0,fourString2);
			intToFourString(IC+100,fourString1);
			fprintf(files[0],"%s\t\t%s\t\t%c\n",getPastZeroes(fourString1),fourString2,'e');
			if(strcmp(np->type,".extern")==0)
				fprintf(files[ext],"%s\t\t%s\n",word,getPastZeroes(fourString1));
			else
				fprintf(files[ent],"%s\t\t%s\n",word,getPastZeroes(fourString1));
		}
		else if(word[0]=='#')
		{
			IC++;
			intToFourString(atoi(word+1),fourString2);
			intToFourString(IC+100,fourString1);
			fprintf(files[0],"%s\t\t%s\t\t%c\n",getPastZeroes(fourString1),fourString2,'a');
		}
		else if(!isRegister(word))
		{
			error++;
			printf("line %d: this      %s      not define\n",lineNum,word);
			break;
		}
		flag++;
		
	}while(getAddressingMethod(s)=='2' && flag==2);
}


void printToEX(char word[],FILE *files[])
{
	enum fl{ob,ent,ext};
	char fourBase[11];
	Nptr p=NULL;
	
	if(p=findInTree(&exSymbolTree,word))
	{
		intToFourString(IC+100,fourBase);
		if(strcmp(p->type,".entry")==0)
			fprintf(files[ent],"%s\t\t%s\n",word,getPastZeroes(fourBase));	
		else
			fprintf(files[ext],"%s\t\t%s\n",word,getPastZeroes(fourBase));
	}	
}