
#include "commands.h"
int lineNum=0;
Nptr symbolTree=NULL;
Nptr exSymbolTree=NULL;
int error=0;
int DC=-1;/* I initiate DC and IC to -1 because the "adding" is simpler that way. */
int IC=-1;

cmd commands[]=
{
	{"mov","0123","123","0000",2},
	{"cmp","0123","0123","0001",2},
	{"add","0123","123","0010",2},
	{"sub","0123","123","0011",2},
	{"not","","123","0100",1},
	{"clr","","123","0101",1},
	{"lea","123","123","0110",2},
	{"inc","","123","0111",1},
	{"dec","","123","1000",1},
	{"jmp","","123","1001",1},
	{"bne","","123","1010",1},
	{"red","","123","1011",1},
	{"prn","","0123","1100",1},
	{"jsr","","1","1101",1},
	{"rts","","","1110",0},
	{"stop","","","1111",0},
	{"NULL"}
};
Register registers[]=
{
	{"r0","000"},{"r1","001"},{"r2","010"},{"r3","011"},{"r4","100"},{"r5","101"},{"r6","110"},{"r7","111"},{"NULL","\0"}	
};



