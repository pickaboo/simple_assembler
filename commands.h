#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define CMD_ARRAY 10/* characters of command, though "stop" is the longest */
#define MAX_WORD 30 /*every token size*/
#define MAX_LINE 100/*max chars in a given line.*/
#define RAW_WORDS 4 /* maximum "chunks" of string, i.e, in command line with two operands and label at beginning.   */
#define BITS_NUM 20/*can not see any portability for this variable. represent the virtual assembler input (in binary).*/

typedef struct
{
	char cmdString[CMD_ARRAY];
	char sourceMethods[CMD_ARRAY];
	char destinationMethods[CMD_ARRAY];
	char code[CMD_ARRAY];
	int numOfOperands;
} cmd;

typedef struct node *Nptr;

typedef struct node/*this node store the information needed for a symbol/label/external symbol.*/
{
	int address;/*the address can be IC or DC or 0 in case of .entry or .extern*/
	char symbol[MAX_WORD];/*the symbol string itself.*/
	char type[CMD_ARRAY];/*can be label in command line than put "code", else .extern, .entry, .data or .string  */
	char content[MAX_WORD];/*if .data or .string copy the the data here, else put '\0' */
	Nptr next;
}Node;

typedef struct
{
	char reg[CMD_ARRAY];
	char regBits[CMD_ARRAY];
} Register;

cmd* getCommand(char s[]);/*get  pointer to all the specific criteria for the command s. if s not a command, return NULL.*/
char *getRegister(char s[]);/*return pointer if s legit register.*/
Nptr createNode(int add,char sym[],char ty[], char con[]);/*create node from a symbol and relevant info if needed (if the symbol belong to .data for example )*/
void addToTree(Nptr *Treeptr, Nptr Nodeptr);/*add symbol(and any other relevant info) as node.*/
void deleteTree(Nptr *tree);/*delete the tree.*/
Nptr findInTree(Nptr *tp,char *s);/*return NULL if the symbol  not in tree, otherwise return pointer to its node.*/
void binaryStringToFourString(char b[], char f[]);/*translate from binary string to four base string (array b need to be exactly twice as long of array f).*/
void intToFourString(int a, char f[]);
char *isQuotedLine(char s[]);/*determine if the line is double quoted line, i.e their may be empty space before,in and after the double quoted line. */
int isDiverse(char *s);/*check if the string has diverse index addressing(only syntacticly).
notice: between the braces  can appear not a valid label like {3eee} b*/
int isSymbol(char *s);/*check if the symbol is legit (only syntacticly)*/
Register *isRegister(char *s);/*if s is a legit register.*/
int isInt(char s[]);/*contains only digit and/or leading + or -.*/
int isString(char s[]);/* if s contains only letters return 1 else return 0*/
int countWords(char s[]);/*count any thing legal that not register*/
int isNumbers(char s[]);/*if the string contains only  " ," or integers. return number of integers.*/
int getAddressingMethod(char s[]);
/*this function use the 3 function above to determine (syntactically but not semantically)
 * the addressing method number. very important! return as ASCII char code like '0','1','2' or'3'*/

char *fromDecimalToBinary(int a);/*service function ,return a binary string representation of decimal integer. the representation is limited to 2^(BITS_NUM-1)     */
int isLabel(char w[]);/*prepare the word to function isSymbol*/
int bitsUseInOperands(char s[], int numOfOperands);/*to decide the proper bits use due to the number of operands like /0/1/1 for 2 operands.*/
void copyEnd(char to[], char of[]);/*put in s a string from of that start with /  */

void openFile(char *s);/*check and open the assembly file for instruction (a (something).as file)*/
int create3Files(char from[],FILE *files[]);/*create and open the necessary files in order to write the output of the program (.ob  .ext  .ent files).*/
void closeAllFiles(FILE *f,FILE *files[]);
void readLine(char s[]);/*read line by line from .as file. then s is tokenized into 4 tokens at most. doing so the function alert of "rough" mistakes in s.
than transfer the the tokens to firstScan function */
void codeLine(char s[], FILE *files[]);/* second scan of the line(file). here the actual writing is done. 
also, checking for semantic mistake like symbol as operand not found and such.*/
void firstScan(char *s[]);/* this function get its argument s, from readLine function.in this function we analyze s by its tokens as written in page 28 first part. */
void stringLine1(char *s[], int flag);/*type .string, checking it*/
void dataLine1(char *s[], int flag);/*type .data, checking it, generic to complicated.*/
void exLine1( char *s[],int flag,int type);/*sub routine of function firstScan. analyze all the "hanhaya" sentence. 
arguments:
s-is the all line as tokens
flag- if 1 the first token is label,else 0
type- 0: .data   1: .string    2: .extern 3: .entry
*/
void commandLine1(char *s[],int flag);/* sub routine of function firstScan. if s as tokens is a command line. flag is for label or not*/
void updateAddress(int ad);/*update address of extern and string after the first scan*/
void commandLine2(char *words[],int flag,FILE *files[]);/*part of the second scan. we know the word is a command line. 
this is the "father"  of second scan, i.e, page 28 bottom. */
void printToEX(char word[],FILE *files[]);/*this function responsible to to check if word is in external symbol and print it according to affiliation. */

void getStringInOutBraces(char from[], char to[],int flag);/*simple sub routine to differentiate token like: frf{gg} to 2 tokens.*/
void setRegisters(char **s,cmd *p,char bits[]);/*set the binary digits of the registers in bits array, which is the ongoing "program order". p decide the active registers.  */
void setAddressing(char **s,cmd *p,char bits[]);/*set the binary digits of the addressing method in bits array, which is the ongoing "program order".*/
void setOpcode(cmd *p,char bits[]);/*set the binary digits of the opcode in bits array, which is the ongoing "program order".*/
void setTypeAndComb(char **s, cmd *p, char bits[]);/*set(change) the argument bits which is a string representation of binary machine order (page 13). 
put the proper bits in type and comb "slots" of argument bits. */
void setOperands(char s[],FILE *files[]);/*analyze and print correctly the operand. the operand method is legal syntactically but here we check for definition and of what type. */
void printDataOrString(FILE *);/*translate and print the data in .data or .string to the .ob file */
char *getPastZeroes(char s[]);/*service function, s is a numeric representation but may contain leading zeros 
so this function move the pointer to first non zero.*/




