#include "commands.h"
extern int error;
extern Nptr symbolTree;
extern Nptr exSymbolTree;
extern  int DC;
extern int IC;
extern int lineNum;

/*
 * check if s is prefix of .as file. if so, implement the 2 scan algorithm.
 */
void openFile(char *s)
{
	char IC_String[11];
	char DC_String[11];
	
	FILE *f;
	FILE *files[3];
	
	char header[MAX_WORD];
	char ss[MAX_WORD];
	char line[MAX_LINE];
	
	strcpy(header,s);
	strcpy(ss,s);
	strcat(header,".as");
	
	if(f=fopen(header,"r"))
		while(fgets(line,MAX_LINE,f))
			readLine(line);/*this is the first scan*/
	else printf("cant open file or not exists.\n");
	
	if(fclose(f))
		printf("cant close file for some reason...\n");
	
	if(error==0)
	{
		if(f=fopen(header,"r"))
		{	
			if(create3Files(s,files))/*create the additional files only if needed.*/
			{
				
				lineNum=0;
				IC++;
				DC++;
				intToFourString(IC,IC_String);
				intToFourString(DC,DC_String);
				fprintf(files[0]," %s\t\t%s\n",getPastZeroes(IC_String),getPastZeroes(DC_String));/*the first line in the .ob file*/
				/*the first address of DC is 0 */
				updateAddress(IC);/*for the .data and .string*/
			
				IC=-1;
				
				fclose(f);
				f=fopen(header,"r");
				
				while(fgets(line,MAX_LINE,f))
					codeLine(line,files); /*this is the second scan*/
					
					printDataOrString(files[0]);/*translate and print the data in .data or .string to the .ob file */
				
					closeAllFiles(f,files);	
					deleteTree(&exSymbolTree);
					deleteTree(&symbolTree);
				
					if(error==0)
					{
						printf("success... (-:\n");
					}
					else printf("mistakes in second scan )-:\n");
			}	
			else printf("cant open files or not exists.\n");
		}	
			
	}
	else printf("too much mistakes in first scan  )-: \n");
	/*if there is another .as file to process in the shell these variable must be initiated*/
	exSymbolTree=NULL;
	symbolTree=NULL;
	lineNum=0;
	DC=-1;
	IC=-1;
	error=0;	
}
/*
 this function take its argument, s, and turn it to tokens. make only few general tests for soundness (like number of tokens). 
 */
void readLine(char s[])
{
		int j=0;
		lineNum++;
		int i=0;
		char *words[RAW_WORDS+1];
		while(i<=RAW_WORDS)
			words[i++]=NULL;
		i=0;
		if(strstr(s,".data"))/*if .data sentence.*/
		{
			
			words[i]=strtok(s, " \t\n");
			while(i<RAW_WORDS)
				words[++i]=strtok(NULL, " \t\n");
		}
		else
		{
			words[i]=strtok(s, " ,\t\n");
			while(i<RAW_WORDS)
			{
				if(words[i] && strcmp(words[i],".string")==0 && (i==0 || i==1))/* .string line*/
				{
					
					words[++i]=strtok(NULL,"\n");
					if(words[i] && isQuotedLine(words[i]))
					{
						words[i]=isQuotedLine(words[i]);
						
						break;
					}
					else
					{
						error++;
						printf("line: %d string line not good\n",lineNum);
					}	
				}
				else 
					words[++i]=strtok(NULL, " ,\t\n");/*i.e, not a .string line nor .data line*/
			}		
		}
		
		if(words[0]==NULL ||words[0][0]==';');/*empty line or remark*/
			else if(words[RAW_WORDS])
			{
				printf("line %d: too much arguments\n",lineNum);
				error++;
			}
			else /*if(error==0)*/
					firstScan(words);/* the algorithm of page 28 */
					
}
/*
 * reminder: files[0] for ob, files[1] for .entry, files[2] for .extern.
 * this is second scan as written in page 28.
 * s is the checked line. files is file array to be printed on.
 */
void codeLine(char s[],FILE *files[])
{

	char del[]=" ,\n";
	lineNum++;
	int flag=0;/*label or not*/
	char label[MAX_WORD];
	char *words[4];
	Nptr p1=NULL;
	Nptr p2=NULL;
	char fourString[11];
	
	words[0]=strtok(s,del);
	words[1]=strtok(NULL,del);
	if(words[0] && words[0][0]!=';')/*not empty line or remark*/
	{
		if(isLabel(words[0]))
		{
			flag=1;
			strcpy(label,words[0]);
			label[strlen(label)-1]='\0';
		}
		
		if(strcmp(words[flag],".extern")==0 || strcmp(words[flag],".entry")==0 || strcmp(words[flag],".string")==0 || strcmp(words[flag],".data")==0);/*do nothing*/
		else
		{	
			words[2]=strtok(NULL,del);
			words[3]=strtok(NULL,del);
			commandLine2(words,flag,files);
		}
		
	}
}
/*
 the first scan on tokenized line.
 */
void firstScan(char *s[])/* up to s[3].*/
{
	enum types{data,str,ext,ent};/*for the 4 hanhaya types*/
	
	int flag=0;
	if(isLabel(s[0]))/*check if label */
	{
		s[0][strlen(s[0])-1]='\0';
		flag=1;
	}
	
	if((strcmp(s[flag],".string")==0 || strcmp(s[flag],".data")==0 || strcmp(s[flag],".extern")==0 || strcmp(s[flag],".entry")==0) && s[flag+2])
	{
		error++;
		printf("line %d: too much argument.\n",lineNum);
	}
	else if(strcmp(s[flag],".string")==0)
			exLine1(s,flag,str);
	else if(strcmp(s[flag],".data")==0)
		exLine1(s,flag,data);
	else if(strcmp(s[flag],".extern")==0)
		exLine1(s,flag,ext);
	else if(strcmp(s[flag],".entry")==0)
		exLine1(s,flag,ent);
	else	 if(flag==0 && s[3])
	{
		error++;
		printf("line %d: too much argument.\n",lineNum);
	}/*if not hanhaya it can be only command line*/
		else commandLine1(s,flag);
}


/*this function is part of the first scan.
 according to the type of the line the checking is done.
 here i use  the DC to count and allocate enough space to the data. the "symbol table" are the extern trees.
 the data is stored dynamically in a node as described in the header file.*/
void exLine1(char *s[],int flag, int type)
{
	enum types{data,str,ext,ent};
	
	if(s[1+flag])
	{	
			/*some sort of hanhaya line.*/
		if((isNumbers(s[1+flag]) && data==type) || (isString(s[1+flag])!=-1 && str==type) || (isSymbol(s[1+flag]) && ext==type) || (isSymbol(s[1+flag]) && ent==type))
		{
			if(s[2+flag])
			{
				error++;
				printf("line %d:  too much argument to %s .\n",lineNum,s[0+flag]);
			}
			else
			{
				if((flag && findInTree(&symbolTree,s[0]) && (str==type || data==type)) || ( findInTree(&exSymbolTree,s[1+flag]) && (ext==type || ent==type)))
				{
					printf("line %d:  label already exists (if it  extern or entry line it means the label after).\n",lineNum);
					error++;
				}
				else /*with or without flag*/
				{	
					if(flag)
					{
						if(str==type || data==type)
						{
							DC++;
							addToTree(&symbolTree,createNode(DC,s[0],s[0+flag],s[1+flag]));
						}
						else
							addToTree(&exSymbolTree,createNode(0,s[1+flag],s[0+flag],""));
					}	
					else
					{
						if(str==type || data==type)
						{
							DC++;
							addToTree(&symbolTree,createNode(DC,"",s[0+flag],s[1+flag]));
						}
						else
							addToTree(&exSymbolTree,createNode(0,s[1+flag],s[0+flag],""));
					}
					if(str==type )
						DC+=isString(s[1+flag]); /*for the \0 */
					else if(data==type)
							DC+=isNumbers(s[1+flag])-1;
				}
			}
		}
		else
		{
			printf("line %d: argument %d not valid.\n",lineNum,(1+flag));
			error++;
		}
	}
	else
	{
		printf("line %d:  not enough arguments for %s.\n",lineNum,s[0+flag]);
		error++;
	}	
}
/*this function is part of the first scan.
 check if this line is a command line.
 here i use  the IC to count and allocate enough space to the labels.
 flag=1 says there is a label at beginning.*/
void commandLine1(char *s[],int flag)
{
	IC++;
	int numOfOperands=0;
	int command=0;
	char word1[MAX_WORD];
	char word2[MAX_WORD];
	char bits[MAX_WORD];
	cmd *p=NULL;
	
	strncpy(word1,s[flag],3);/* command*/
	word1[3]='\0';
	strncpy(word2,s[flag],4);/* command stop.*/
	word1[4]='\0';
	
	if(s[1+flag])
		numOfOperands++;
	if(s[2+flag])
		numOfOperands++;
	
	
	if(p=getCommand(word1))
		command=1;
	else if(p=getCommand(word2))
			command=1;
	if(command)
	{
		if(flag && findInTree(&symbolTree,s[0]))/*to avoid double meaning to a label.*/
		{
			error++;
			printf("line %d: argument %d, label already exists.\n",lineNum,(flag));
		}	
			if(flag)
				addToTree(&symbolTree,createNode(IC,s[0],"code",""));
			
			copyEnd(bits, s[flag]);/*for bits string like mov  -->  /1/1/1 */
		
			if(p->numOfOperands==numOfOperands)
			{
				if(bitsUseInOperands(bits,numOfOperands)==-1)/*like /1/1/1 in mov/1/1/1*/
				{
					error++;
					printf("line %d: bits  after command not valid.\n",lineNum);
				}
				else if(numOfOperands>0)
					{
						if(numOfOperands==1 && strchr(p->destinationMethods,getAddressingMethod(s[1+flag])) )
							IC=IC+countWords(s[1+flag]);
						else if(numOfOperands==2 && strchr(p->sourceMethods,getAddressingMethod(s[1+flag]))  && strchr(p->destinationMethods,getAddressingMethod(s[2+flag])))
						{
							IC=IC+countWords(s[1+flag]);
							IC=IC+countWords(s[2+flag]);
						}
						else
						{
							error++;
							printf("line %d: addressing source/destination methods not valid.\n",lineNum);
						}
					}
					/*do nothig for only command stop or rts.*/		
		}
		else
		{
			error++;
			printf("line %d: too many/few operands for command.\n",lineNum);
		}
	}
	else
	{
		error++;
		printf("line %d: argumenr %d is not command\n",lineNum,(1+flag));
	}	
}
/*
 part of the second scan.
 here i built the "binary string" for the "compiler". 
 words- argument is the tokens of a single command line.
 flag- if there is label at the beginning.
 files- files to write on.
 */
void commandLine2(char **words,int flag,FILE *files[])
{
	char fourString1[11];
	char fourString2[11];
	char bitString[BITS_NUM+1];/*the empty binary string*/
	int i;
	char command[CMD_ARRAY];
	cmd *p=NULL;
	
	strncpy(command,words[flag],3);
	p=getCommand(command);

	if(p==NULL)
		p=getCommand("stop");
			
	for(i=0;i<BITS_NUM;i++)/*initial*/
		bitString[i]='0';
	bitString[BITS_NUM]='\0';
	
	enum fl{ob,ent,ext};
	IC++;
	
	if(flag)
	{
		words[0][strlen(words[0])-1]='\0';
		printToEX(words[0],files);	/*if the label is symbol in external symbol*/
	}
	/* for this 4 functions to be clear there is a need  for diagram like in page 13 because the bits location is key. */
	setTypeAndComb(words+flag, p, bitString);
	setOpcode(p,bitString);
	setAddressing(words+flag,p,bitString);
	setRegisters(words+flag,p,bitString);
	
	intToFourString(IC+100,fourString1);/*the address in four base.*/
	binaryStringToFourString(bitString,fourString2);/*the compiler command is translated to the four base string.*/
	
	fprintf(files[0],"%s\t\t%s\t\t%c\n",getPastZeroes(fourString1),fourString2,'a');

	if(p->numOfOperands)
	{
		setOperands(words[1+flag],files);/*print to files*/
		if(p->numOfOperands==2)
			setOperands(words[2+flag],files);
	}
	
	
	
}
void setTypeAndComb(char **s, cmd *p, char bits[])
{
	int bitType;
	int index=strlen(p->cmdString);
	bitType=bitsUseInOperands(s[0]+index,p->numOfOperands);
	if(bitType>=10)/*i gradually change the bits from 0 to 1 if needed.*/
		bits[3]='1';
	if(bitType>=110)
		bits[18]='1';
	if(bitType==11 ||bitType==111|| bitType==101)
		bits[19]='1';
}
void setOpcode(cmd *p,char bits[])/*put opcode in string*/
{
	int i,j;
	char *s=p->code;
	for(i=4,j=0;s[j];i++,j++)
		bits[i]=s[j];
}
void setAddressing(char **s,cmd *p,char bits[])
{
	int opr=p->numOfOperands;
	int method;
	if(opr==2)
	{
		method=getAddressingMethod(s[1]);
		if(method>='2')
			bits[8]='1';
		if(method=='1' ||method=='3')
			bits[9]='1';
		method=getAddressingMethod(s[2]);
		if(method>='2')
			bits[13]='1';
		if(method=='1' ||method=='3')
			bits[14]='1';
	}
	else if(opr==1)
	{
		method=getAddressingMethod(s[1]);
		if(method>='2')
			bits[13]='1';
		if(method=='1' ||method=='3')
			bits[14]='1';
	}
}
void setRegisters(char **s,cmd *p,char bits[])
{
	
	int flag=0;
	int regi=0;
	int opr=p->numOfOperands;
	Register *rp=NULL;
	int i,j;
	int method;
	char reg[MAX_WORD];
	if(opr==2 || opr==1)
	{
		
		if(opr==1)
			regi=5;/*the space between register source and register destination. */
		if(opr!=0)
		{	

			while(flag<2)/* for source register and destination register.*/
			{	
				if(rp=isRegister(s[1+flag]))
				{
					
					for(i=10+regi,j=0;(rp->regBits)[j];i++,j++)
						bits[i]=(rp->regBits)[j];
				}
				else if(getAddressingMethod(s[1+flag])=='2')/*there is register between braces.*/
				{
					
					getStringInOutBraces(s[1+flag],reg,2);
					if(rp=isRegister(reg))
						for(i=10+regi,j=0;(rp->regBits)[j];i++,j++)
							bits[i]=(rp->regBits)[j];
				}
				if(opr==1)
					break;
				else
				{
					flag++;
					regi=5;
				}
				
			}	
		}
		
	}
	
}
void getStringInOutBraces(char from[], char to[],int flag)
{
	char data[MAX_WORD];
	strcpy(data,from);
	strcpy(to,strtok(data," {}"));
	if(flag==2)
		strcpy(to,strtok(NULL," {}"));
}

















