all: commands.o main.o services.o actions.o prints.o
	gcc -o assembler  main.o actions.o commands.o services.o prints.o
commands.o: commands.c commands.h
	gcc -c commands.c
main.o: main.c commands.h
	gcc -c main.c
services.o: services.c
	gcc -c services.c
actions.o: actions.c commands.h
	gcc -c actions.c
prints.o: prints.c commands.h
	gcc -c prints.c