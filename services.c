#include "commands.h"

extern int error;
extern Nptr symbolTree;
extern Nptr exSymbolTree;
extern  int DC;
extern int IC;
extern int lineNum;
/*check if s is a legit command. if true return pointer else return NULL*/
cmd* getCommand(char s[])
{
	extern cmd commands[];/*the only function that uses this external.*/
	cmd *p=NULL;
	int i;
	for(i=0;strcmp(commands[i].cmdString,"NULL");i++)
		if(!strcmp(s,commands[i].cmdString))
			p=&(commands[i]);
	return p;	
}
/*the fallowing 4 function is regarding the basic operations in a linked list structure. the nodes allowed  are Node in commands.h .*/
void addToTree(Nptr *Treeptr, Nptr Nodeptr)
{
	Nptr p=*Treeptr;
	if(p==NULL)
		*Treeptr=Nodeptr;
	else
	{
		while(p->next)
			p=p->next;
		p->next=Nodeptr;
	}
}
Nptr findInTree(Nptr *tp,char *s)
{
	Nptr p=*tp;
		while(p)
		{
			
			if(strcmp(p->type,s)==0)
				break;
			else
			{	
				if(strcmp(p->symbol,s)==0)
					break;
			}	
				p=p->next;
		}		
	return p;
}
Nptr createNode(int add,char sym[],char ty[], char con[])
{
	Nptr p=NULL;
	if(p=(Nptr)malloc(sizeof(Node)))
	{
		(p->address)=add;
		strcpy(p->symbol,sym);
		strcpy(p->type,ty);
		strcpy(p->content,con);
		p->next=NULL;
	}
	else
		printf("error memory allocation to node symbol\n");
	
	return p;
}
void deleteTree(Nptr *tree)
{
	Nptr curr;
	Nptr sec=NULL;
	
	for(curr=(*tree);curr;curr=sec)
	{	
		sec=curr->next;
		free(curr);
	}
}
/*check: given w as an operand, whether it addressing method is diverse method. if true return 1 else 0*/
int isDiverse(char *w)
{
	int valid=0;
	char s[MAX_WORD];
	strcpy(s,w);
	char *p1;
	char *p2;
	char *p3;
	
	p1=strtok(s,"{}");
	p2=strtok(NULL,"{}");
	p3=strtok(NULL,"{}");
	
	if(p3)
		valid=0;
	else if(!p2)
			valid=0;
	else
	{
		if(isSymbol(p1))
			if(isInt(p2) || isRegister(p2) || isSymbol(p2))/*legit variable in in pars*/
				valid=1;
	}
	return valid;
}
/*check whether s is  a proper symbol syntactically.
 if no return 0.*/
int isSymbol(char *s)
{
	
	
	 int valid=1;
	 int i;
	 if(!isalpha(s[0]))
		 valid=0;
	 else if(isRegister(s))
		 valid=0;
	 else if(getCommand(s))
		 valid=0;
	 else
	 {
			for(i=0;s[i];i++)
				if(!isalnum(s[i]))
				{	
					valid=0;
					break;
				}	
	 }
	 
	return valid; 
}
/*if s is register, return its address else NULL*/
Register *isRegister(char *s)
{
	extern  Register registers[];
	int i;
	Register *p=NULL;
	for(i=0;strcmp(registers[i].reg,"NULL");i++)
		if(!strcmp(s,registers[i].reg))
			p=&registers[i];
	return p;
}
/*given s is an operand, this function return as char its value like in page 15, if not return '!'*/
int getAddressingMethod(char s[])
{
	int i=33;/* '!' */
	char *p;
	if(s[0]=='#')
	{	
		p=&s[1];
		if(isInt(p))
			i='0';
	}
	else
	{
		if(isDiverse(s))
				i='2';
		else
		{
			if(isRegister(s))
				i='3';
			else
			{
				if(isSymbol(s))
					i='1';
			}
		}
	}
	return i;
}
/*check if s is a proper integer.*/
int isInt(char *s)
{
	int valid=1;
	int i;
	if(s[0]=='+'||s[0]=='-'||isdigit(s[0]))
	{	
		for(i=1;s[i];i++)
		{
			if(!isdigit(s[i]))
			{
				valid=0;
				break;
			}
		}
	}
	else
		valid=0;
	
	return valid;
}
/*simple function that transform a binary string representation (b) to four base representation (f). this is a sub routine of the next function.*/
void binaryStringToFourString(char b[], char f[])
{
	int i;
	int j;
	for(i=0,j=0;b[i]!='\0';i+=2,j++)
	{
		if(b[i]=='0' && b[i+1]=='0')
			f[j]='0';
		else if(b[i]=='0' && b[i+1]=='1')
				f[j]='1';
		else if(b[i]=='1' && b[i+1]=='0')
				f[j]='2';
		else if(b[i]=='1' && b[i+1]=='1')
				f[j]='3';
	}
	f[j]='\0';
}
/*this function transform a integer property (a) to a four base string representation (f). note that the max number is 2^20-1*/
void intToFourString(int a, char f[])
{
	char *s;
	int i;
	int j;
	char b[BITS_NUM+1];/*string of 20 bits and \0 */
	s=fromDecimalToBinary(a);
	strcpy(b,s);
	free(s);
	for(i=0,j=0;b[i]!='\0';i+=2,j++)
	{
		if(b[i]=='0' && b[i+1]=='0')
			f[j]='0';
		else if(b[i]=='0' && b[i+1]=='1')
				f[j]='1';
		else if(b[i]=='1' && b[i+1]=='0')
				f[j]='2';
		else if(b[i]=='1' && b[i+1]=='1')
				f[j]='3';
	}
	f[j]='\0';
}
/*this function return a pointer to first appearance of non zero char, thus getting "nice" representation. */
char *getPastZeroes(char s[])
{
	int i;
	for(i=0;s[i];i++)
		if(s[i]!='0')
			return s+i;
	return NULL;	
		
}
/* this function transform a integer property (a) to binary string representation and return it.*/
char *fromDecimalToBinary(int a)
{
	int bits=BITS_NUM;
	int pos=0;
	if(a>0)
		pos=1;
	else
		a=a*-1;
	unsigned int b=a;
	int i;
	int index=0;
	char *s=NULL;
	if(s=(char*)malloc((bits+1)*sizeof(char)))
	{
		s[bits]='\0';
		for(i=bits-1;i>-1;i--)
		{
			if(b>>i & 01)
			{
				if(pos)
				s[index]='1';
				else
					s[index]='0';
			}	
			else
			{
				if(pos)
				s[index]='0';
				else
					s[index]='1';
			}	
			index++;
		}
		if(pos==0)/* if a is negative we need to make  2 complement method.*/
		{
			index=bits-1;
			while(index>=0)
			{
				if(s[index]=='0')
				{
					s[index]='1';
					break;
				}
				else
				{
					s[index]='0';
					index--;
				}
			}
		}
	}
	else
		printf("cant allocate memory to string of binary\n");
	return s;
}
/*check the backlash string with the bits. if the return is -1 than error*/
int bitsUseInOperands(char s[], int type)
{
	int x=-1;
	enum types{noOperand,oneOperand,twoOperands};
	switch(type)
	{
		case noOperand:
			if(strcmp(s,"/0")==0)
				x=0;
			break;
		case oneOperand:
			if(strcmp(s,"/1/0")==0)
				x=10;
			else if(strcmp(s,"/1/1")==0)
					x=11;
			else if(strcmp(s,"/0")==0)
					x=0;
		case twoOperands:/*removed the break */
			if(strcmp(s,"/0")==0)
				x=0;
			else if(strcmp(s,"/1/0/0")==0)
				x=100;
			else if(strcmp(s,"/1/0/1")==0)
					x=101;
			else if(strcmp(s,"/1/1/0")==0)
					x=110;
			else if(strcmp(s,"/1/1/1")==0)
					x=111;
			break;	
	}
	return x;
}
/*this function create the necessary files to write on, using from as a prefix.*/
int create3Files(char from[],FILE *files[])/*remember to check if the type exist. if not- do not create file*/
{
	extern Nptr exSymbolTree;
	int status=1;
	char to[MAX_WORD];
	strcpy(to,from);
	
	if((files[0]=fopen(strcat(to,".ob"),"w"))==NULL)
		status=0;
		
	strcpy(to,from);
	
	if(findInTree(&exSymbolTree,".entry"))
	{	
		
		if(!(files[1]=fopen(strcat(to,".ent"),"w")))
		{	
			status=0;
			printf("can not create file %s\n",to);
		}
	}	
	else files[1]=NULL;
	
	strcpy(to,from);
	
	if(findInTree(&exSymbolTree,".extern"))/*create only if */
	{	
		
		if((files[2]=fopen(strcat(to,".ext"),"w"))==NULL)
		{	
			status=0;
			printf("can not open file %s\n",to);
		}
	}	
	else files[2]=NULL;
	
	return status;
}
/*close all the files. f is the .as file and files is array of files.*/
void closeAllFiles(FILE *f,FILE *files[])
{
	fclose(f);
	fclose(files[0]);
	if(files[1]!=NULL)
		fclose(files[1]);
	if(files[2]!=NULL)
		fclose(files[2]);
}
/*check if s is the content of a .string line.*/
int isString(char s[])
{
	int l=strlen(s);
	int count=0;
	int i;
	if(s[0]=='\"' && s[l-1]=='\"')
	{	
		for(i=1;i<l-1;i++)
				count++;
	}
	else
		count=-1;
	
	return 	count;
}

int countWords(char w[])
{
	int type=0;
	char s[MAX_WORD];
	strcpy(s,w);
	int count=0;
	char *p=NULL;
	type=getAddressingMethod(s);

	if(type=='0' || type=='1')
		count=1;
	else if(type=='2')
	{
		p=strtok(s,"{}");
		if(isSymbol(p))
			count++;
		while(p=strtok(NULL,"{}"))
			if(isSymbol(p)|| isInt(p))
				count++;	
	}
	return count;
}
/*this function is utilized in a .data command. check if the string s contains only integers.
 if valid, the function return the number of them, else return 0.*/
int isNumbers(char s[])
{
	char line[MAX_WORD];
	strcpy(line,s);
	int count=0;
	char *num;
	num=strtok(line," ,");
	if(isInt(num))
	{
		count++;
		while(num=strtok(NULL," ,"))
		{
			if(isInt(num))
				count++;
			else
			{
				count=0;
				break;
			}
		}
			
	}
	return count;
}
/*this function is utilized in a .string command. 
 if valid, return pointer to the beginning , else return NULL.*/
char *isQuotedLine(char s[])
{
	
	char *valid=NULL;
	int par=0;
	int i;
	for(i=0;s[i];i++)
	{
		if(par==0)
		{
			if(s[i]=='\"')
			{
				par=1;
				valid=s+i;
			}	
			else if(s[i]!=' ')
				break;
		}
		else if(par==1)
		{
			if(s[i]=='\"')
			{
				par=2;
			}
				
		}
		else if(par==2)
		{
			if(s[i]==' ' || s[i]=='\0')
				s[i]='\0';
			else if(s[i]!=' ')
			{
				valid=NULL;
				break;
			}
		}
	}

	if(!(par==2 && valid))
		valid=NULL;
	return valid;
		
		
}
/*separate the command string from the bit string.*/
void copyEnd(char to[], char from[])
{
	int i;
	int dash=0;
	for(i=0;from[i];i++)
	{
		if(from[i]=='/')
		{
			strcpy(to,from+i);
			break;
		}
	}
}
int isLabel(char w[])
{
	int label=0;
	int length=0;
	char s[MAX_WORD];
	strcpy(s,w);
	length=strlen(s);
	if(s[length-1]==':')
	{
		s[length-1]='\0';
		if(isSymbol(s))
			label=1;
	}
	return label;
}
/*this function is utilized after the first scan has finished. so all the DATA addresses is updated, i.e get the address after the command last address (ad).*/
void updateAddress(int ad)
{
	extern Nptr symbolTree;
	Nptr p=NULL;
	for(p=symbolTree;p;p=p->next)
	{
		if(strcmp(p->type,".data")==0 || strcmp(p->type,".string")==0)
			p->address+=ad;	
	}
}	



















